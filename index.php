<?php
	require_once('animal.php');
	require_once('frog.php');
	require_once('ape.php');

	$sheep = new Animals("shaun");
	echo "Animal's Name : " . $sheep->name . "<br>" ; // "shaun"
	echo "Animal's Legs : " . $sheep->legs . "<br>";// 4
	echo "Cold Blooded : " . $sheep->cold_blooded .  "<br><br>"; // "no"


	$kodok = new Frog("buduk");
	echo "Animal's Name : " . $kodok->name . "<br>" ;
	echo "Animal's Legs : " . $kodok->legs . "<br>";
	echo "Cold Blooded : " . $kodok->cold_blooded . "<br>";
	echo  $kodok->jump(). "<br><br>";

	$sunggokong = new Ape("Kera Sakti");
	echo "Animal's Name : " . $sunggokong->name . "<br>" ;
	echo "Animal's Legs : " . $sunggokong->legs . "<br>";
	echo "Cold Blooded : " . $sunggokong->cold_blooded . "<br>";
	echo  $sunggokong->yell();



?>